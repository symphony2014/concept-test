﻿using ExcelImport.Models;
using Qmaker.DAL;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Entity.Core.Objects;
using System.Data.OleDb;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ExcelImport.Controllers
{
    public class HomeController : Controller
    {
        public string Connection { get {
                return ConfigurationManager.ConnectionStrings["intel"].ToString();
            } }
        public string DeleteScript(DataTable table, string tableName)
        {
            string deleteSql = string.Empty;


            switch (tableName)
            {
                case "msp_rawdata_diy_iu":
                    foreach (DataRow item in table.Rows)
                    {
                        deleteSql += string.Format("  delete msp_rawdata_diy_iu where yr_qtr_int_nbr='{0}' and yr_qtr_rd={1} and stor_id ='{2}' and SessionName=N'{3}'  ", item["yr_qtr_int_nbr"], item["yr_qtr_rd"], item["stor_id"], item["SessionName"]);
                    }
                    break;
                case "msp_rawdata_mr_iu":
                    foreach (DataRow item in table.Rows)
                    {
                        deleteSql += string.Format(" delete msp_rawdata_mr_iu where yr_qtr_int_nbr='{0}' and yr_qtr_rd={1} and GID ={2} and SessionName=N'{3}'  ", item["yr_qtr_int_nbr"], item["yr_qtr_rd"], item["GID"], item["SessionName"]);
                    }
                    break;
                case "tb_intel_data_XOEM":
                    foreach (DataRow item in table.Rows)
                    {
                        deleteSql += string.Format(" delete tb_intel_data_XOEM where Quarter={0} and shopID={1} and SessionName=N'{2}' ", item["Quarter"], item["shopID"], item["SessionName"]);
                    }
                    break;
                default:
                    break;
            }
            return deleteSql;
        }
        public ActionResult Index()
        {
            ViewBag.Title = "Home Page";

            return View();
        }
        public ActionResult FileUpload(bool isNewData, string tableName)
        {
            int result;
            ErrorMessage message = new ErrorMessage();
            var file = Request.Files[0];
            if ((file.ContentType != "application/vnd.ms-excel") && (file.ContentType != "application/ms-excel") && (file.ContentType != "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"))
            {
                return Json("");
            }
            else
            {
                string filePath = string.Empty;
                filePath = "/UploadFile/" + System.DateTime.Now.Year.ToString() + System.DateTime.Now.Month.ToString() + System.DateTime.Now.Day.ToString() + System.DateTime.Now.Hour.ToString() + System.DateTime.Now.Minute.ToString() + System.DateTime.Now.Second.ToString() + "." + file.FileName.Split('.')[1];
                file.SaveAs(Server.MapPath(filePath));
                string SuffixName = file.FileName.Split('.')[1];
                string strConn = "";
                if (SuffixName == "xls")
                {
                    //Offic2003连接字符串
                    strConn = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + Server.MapPath(filePath) + ";Extended Properties='Excel 8.0;HDR=Yes;IMEX=1'";

                }
                else if (SuffixName == "xlsx")
                {
                    //Offic2010/2007连接字符串
                    strConn = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + Server.MapPath(filePath) + ";Extended Properties='Excel 8.0;HDR=Yes;IMEX=1';";
                }
                else
                {
                    return Json(false);
                }
                OleDbConnection cnnxls = new OleDbConnection(strConn);
                DataSet ds = new DataSet();
                OleDbDataAdapter oada = new OleDbDataAdapter("select * from [Sheet1$]", cnnxls);
                oada.Fill(ds);
                string deleteSql = string.Empty;
                if (!isNewData)
                {
                    deleteSql = DeleteScript(ds.Tables[0], tableName);
                    using (Entities entities = new Entities())
                    {
                        result = entities.Database.ExecuteSqlCommand(deleteSql);
                        message.Message += string.Format("{0}删除了{1}条数据,", tableName, result);
                    }
                }

                //记录更新数据条数
                using (SqlConnection conn = new SqlConnection(Connection))
                {
                    conn.Open();
                    using (SqlTransaction trans = conn.BeginTransaction())
                    {
                        using (SqlBulkCopy copy = new SqlBulkCopy(conn, SqlBulkCopyOptions.Default, trans))
                        {
                            try
                            {
                                //添加
                                copy.DestinationTableName = tableName;
                                foreach (var item in ds.Tables[0].Columns)
                                {
                                    copy.ColumnMappings.Add(item.ToString(), item.ToString());
                                }
                                copy.WriteToServer(ds.Tables[0]);
                                message.IsSuccess = true;
                                message.Message += string.Format("更新了{0}条数据", ds.Tables[0].Rows.Count);
                                trans.Commit();
                            }
                            catch (Exception ex)
                            {
                                message.IsSuccess = true;
                                message.Message = ex.ToString();
                                trans.Rollback();
                            }
                        }
                    }
                }

            }
            return Json(message);
        }
        public ActionResult GetAllQuaters(string tableName)
        {
            string commandText = string.Format("select distinct yr_qtr_int_nbr from {0} order by yr_qtr_int_nbr desc ", tableName);

            if (tableName == "tb_intel_data_XOEM")
            {
                commandText = commandText.Replace("yr_qtr_int_nbr", "Quarter");
            }

            var data = SqlHelper.ExecuteDataset(Connection, CommandType.Text, commandText);
            List<string> quarters = new List<string>();
            foreach (DataRow item in data.Tables[0].Rows)
            {
                quarters.Add(item.ItemArray[0].ToString());
            }
            return Json(quarters);
        }



        public ActionResult Get_diymr_rds(string yr_qtr_int_nbr, string tableName)
        {
            string commandText = string.Format("select distinct yr_qtr_rd from {0} where yr_qtr_int_nbr ={1} order by yr_qtr_rd desc ", tableName, yr_qtr_int_nbr);
            var data = SqlHelper.ExecuteDataset(Connection, CommandType.Text, commandText);
            List<string> diy_rds = new List<string>();
            foreach (DataRow item in data.Tables[0].Rows)
            {
                diy_rds.Add(item.ItemArray[0].ToString());
            }

            return Json(diy_rds);
        }
        public ActionResult Get_diy_stor_ids(string diy_yr_qtr_rd, string yr_qtr_int_nbr)
        {
            string commandText = string.Format("select distinct stor_id from msp_rawdata_diy_iu where yr_qtr_rd={0} and yr_qtr_int_nbr={1} order by stor_id desc", diy_yr_qtr_rd, yr_qtr_int_nbr);
            var data = SqlHelper.ExecuteDataset(Connection, CommandType.Text, commandText);
            List<string> stor_ids = new List<string>();
            foreach (DataRow item in data.Tables[0].Rows)
            {
                stor_ids.Add(item.ItemArray[0].ToString());
            }

            return Json(stor_ids);
        }
        public ActionResult Get_mr_GIDs(string yr_qtr_rd, string yr_qtr_int_nbr)
        {
            string commandText = string.Format("select distinct GID from msp_rawdata_mr_iu where yr_qtr_rd={0} and yr_qtr_int_nbr={1} order by GID　desc", yr_qtr_rd, yr_qtr_int_nbr);
            var data = SqlHelper.ExecuteDataset(Connection, CommandType.Text, commandText);
            List<string> stor_ids = new List<string>();
            foreach (DataRow item in data.Tables[0].Rows)
            {
                stor_ids.Add(item.ItemArray[0].ToString());
            }

            return Json(stor_ids);
        }
        public ActionResult Get_xoem_shopids(string Quarter)
        {
            string commandText = string.Format("select distinct shopID from tb_intel_data_XOEM where Quarter={0} order by shopID desc", Quarter);
            var data = SqlHelper.ExecuteDataset(Connection, CommandType.Text, commandText);
            List<string> stor_ids = new List<string>();
            foreach (DataRow item in data.Tables[0].Rows)
            {
                stor_ids.Add(item.ItemArray[0].ToString());
            }

            return Json(stor_ids);
        }
        public ActionResult Delete_diy(msp_rawdata_diy_iu diy)
        {
            ObjectParameter result = new ObjectParameter("result", typeof(int));
            ErrorMessage message = new ErrorMessage();
            using (Entities entites = new Entities())
            {
                entites.Delete_Diy(diy.yr_qtr_int_nbr, diy.yr_qtr_rd, diy.stor_id, diy.SessionName, result);
            }
            message.Message = string.Format("删除了{0}条数据", Convert.ToInt32(result.Value));
            return Json(message);
        }
        public ActionResult Delete_mr(msp_rawdata_mr_iu mr)
        {
            ObjectParameter result = new ObjectParameter("result", typeof(int));
            ErrorMessage message = new ErrorMessage();
            using (Entities entites = new Entities())
            {
                entites.Delete_mr(mr.yr_qtr_int_nbr, mr.yr_qtr_rd, mr.GID, mr.SessionName, result);
            }
            message.Message = string.Format("删除了{0}条数据", Convert.ToInt32(result.Value));
            return Json(message);
        }
        public ActionResult Delete_xoem(tb_intel_data_XOEM xoem)
        {
            ObjectParameter result = new ObjectParameter("result", typeof(int));
            ErrorMessage message = new ErrorMessage();
            using (Entities entites = new Entities())
            {
                entites.Delete_xoem(xoem.Quarter, xoem.shopID, xoem.SessionName, result);
            }
            message.Message = string.Format("删除了{0}条数据", Convert.ToInt32(result.Value));
            return Json(message);
        }
        public ActionResult Delete(string tableName, string quarter, string waveName)
        {
            ErrorMessage message = new ErrorMessage();
            string commandText = string.Format("delete {0} where yr_qtr_int_nbr={1} and  wave_nm ='{2}' ", tableName, quarter, waveName);
            if (tableName == "tb_intel_data_XOEM")
            {
                commandText = commandText.Replace("wave_nm", "WaveName").Replace("yr_qtr_int_nbr", "Quarter");
            }
            var result = SqlHelper.ExecuteNonQuery(Connection, CommandType.Text, commandText);
            message.IsSuccess = result > 0;
            message.Message = string.Format("删除了{0}条数据", result);
            return Json(message);
        }

        public ActionResult Get_diy_Result(double quarter, string waveName, double storid)
        {
            List<msp_stor_bhvr> list = new List<msp_stor_bhvr>();
            using (Entities entities = new Entities())
            {
                list = (from t in entities.msp_stor_bhvr
                        where t.yr_qtr_int_nbr == quarter && string.Compare(t.wave_nm, waveName, true) == 0 && t.stor_id == storid
                        select t).ToList();
            }
            return Json(list);
        }
        public ActionResult Get_mr_Result(double quarter, double gid_num)
        {
            List<msp_mall_rsllr_bhvr> list = new List<msp_mall_rsllr_bhvr>();
            using (Entities entities = new Entities())
            {
                list = (from t in entities.msp_mall_rsllr_bhvr
                        where t.yr_qtr_int_nbr == quarter && gid_num == t.gid_num
                        select t).ToList();
            }
            return Json(list);
        }
        public ActionResult Get_xoem_Result(double quarter, double shopid, string sessionname)
        {
            List<tb_intel_data_XOEM> list = new List<tb_intel_data_XOEM>();
            using (Entities entities = new Entities())
            {
                list = (from t in entities.tb_intel_data_XOEM
                        where t.Quarter == quarter && string.Compare(t.SessionName, sessionname, true) == 0 && t.shopID == shopid
                        select t).ToList();
            }
            return Json(list);
        }
        public ActionResult msp_stor_bhvr(msp_stor_bhvr tables)
        {
            ErrorMessage message = new ErrorMessage();
            try
            {
                using (Entities entities = new Entities())
                {
                    var model = (from t in entities.msp_stor_bhvr
                                 where t.yr_qtr_int_nbr == tables.yr_qtr_int_nbr && string.Compare(t.wave_nm, tables.wave_nm, true) == 0 && t.stor_id == tables.stor_id
                                 select t).First();
                    model.axx_confg_qty = tables.axx_confg_qty;
                    model.axx_dtd_qty = tables.axx_dtd_qty;
                    model.core_dtd_qty = tables.core_dtd_qty;
                    model.demo = tables.demo;
                    model.e6500_dtd_qty = tables.e6500_dtd_qty;
                    model.Fail_Reason = tables.Fail_Reason;
                    model.ia_confg_qty = tables.ia_confg_qty;
                    model.ia_dtd_qty = tables.ia_dtd_qty;
                    model.IsSuccess = tables.IsSuccess;
                    model.last_upd_dtm = tables.last_upd_dtm;
                    model.pop_sts = tables.pop_sts;
                    model.recom_ia = tables.recom_ia;
                    model.recom_sku = tables.recom_sku;
                    model.remark = tables.remark;
                    //model.stor_id = tables.stor_id;
                    model.stor_nm = tables.stor_nm;
                    // model.wave_nm = tables.wave_nm;
                    model.yr_qtr_int_nbr = tables.yr_qtr_int_nbr;
                    model.yyyymm = tables.yyyymm;
                    entities.SaveChanges();
                    message.IsSuccess = true;
                    message.Message = string.Format("msp_stor_bhvr: 更新一条数据");
                }
            }
            catch (Exception ex)
            {
                message.Message = ex.ToString();
            }

            return Json(message);
        }

        public ActionResult msp_mall_rsllr_bhvr(msp_mall_rsllr_bhvr tables)
        {
            ErrorMessage message = new ErrorMessage();
            try
            {
                using (Entities entities = new Entities())
                {
                    var model = (from t in entities.msp_mall_rsllr_bhvr
                                 where t.yr_qtr_int_nbr == tables.yr_qtr_int_nbr && t.gid_num == tables.gid_num
                                 select t).First();
                    model.axx_dtd_qty = tables.axx_dtd_qty;
                    model.co_nm = tables.co_nm;
                    model.Fail_Reason = tables.Fail_Reason;
                    model.high_end_qty = tables.high_end_qty;
                    model.ia_dtd_qty = tables.ia_dtd_qty;
                    model.isSuccess = tables.isSuccess;
                    model.last_upd_dtm = tables.last_upd_dtm;
                    model.pop_sts = tables.pop_sts;
                    model.scan_sts = tables.scan_sts;
                    model.wave_nm = tables.wave_nm;
                    model.yyyymm = tables.yyyymm;
                    entities.SaveChanges();
                    message.IsSuccess = true;
                    message.Message = string.Format("msp_mall_rsllr_bhvr: 更新一条数据");
                }
            }
            catch (Exception ex)
            {
                message.Message = ex.ToString();
            }

            return Json(message);
        }

        public ActionResult tb_intel_data_XOEM(tb_intel_data_XOEM tables)
        {
            //ErrorMessage message = new ErrorMessage();
            //try
            //{
            //    using (Entities entities = new Entities())
            //    {
            //        var model = (from t in entities.tb_intel_data_XOEM
            //                     where t.Quarter == tables.Quarter && t.shopID == tables.shopID && string.Compare(t.SessionName,tables.SessionName,true)==0
            //                     select t).First();

            //        model.Band = tables.Band;
            //        model.City = tables.City;
            //        model.city_id =tables.city_id;
            //        model.company_id = tables.company_id;
            //        model.dtEditDate = tables.dtEditDate;
            //        model.isSuccess = tables.isSuccess;
            //        model.MSPTypeName = tables.MSPTypeName;
            //        model.OEM_ID = tables.OEM_ID;
            //        model.QID = tables.QID;
            //        model.Result = tables.Result;
            //        model.Score = tables.Score;
            //        model.SessionName = tables.SessionName;
            //        model.StoreAddress = tables.StoreAddress;
            //        model.StoreName = tables.StoreName;
            //        model.WaveName = tables.WaveName;
            //        model.YearMonth = tables.YearMonth;

            //        entities.SaveChanges();
            //        message.IsSuccess = true;
            //        message.Message = string.Format("tb_intel_data_XOEM: 更新一条数据");
            //    }
            //}
            //catch (Exception ex)
            //{
            //    message.Message = ex.ToString();
            //}

            //return Json(message);

            Func<object, object> dbnullFix = (object t) => { if (t == null)return DBNull.Value; else return t; };
            ErrorMessage message = new ErrorMessage();
            string commandText = string.Format("update tb_intel_data_XOEM set Band=@Band,City=@City,city_id=@city_id,company_id=@company_id,isSuccess=@isSuccess,MSPTypeName=@MSPTypeName,[OEM ID]=@OEM_ID,QID=@QID,Result=@Result,Score=@Score,SessionName=@SessionName,StoreAddress=@StoreAddress,StoreName=@StoreName,WaveName=@WaveName,YearMonth=@YearMonth where Quarter=@Quarter and  shopID=@shopID and SessionName =@SessionName ");
            SqlParameter[] paramters = new SqlParameter[] { 
            new SqlParameter("@Band",dbnullFix(tables.Band)) ,
            new SqlParameter("@City",dbnullFix(tables.City)) ,
            new SqlParameter("@city_id",dbnullFix(tables.city_id)) ,
            new SqlParameter("@company_id",dbnullFix(tables.company_id)) ,
            new SqlParameter("@isSuccess",dbnullFix(tables.isSuccess)) ,
            new SqlParameter("@MSPTypeName",dbnullFix(tables.MSPTypeName)) ,
            new SqlParameter("@OEM_ID",dbnullFix(tables.OEM_ID)) ,
            new SqlParameter("@QID",dbnullFix(tables.QID)) ,
            new SqlParameter("@Result",dbnullFix(tables.Result)) ,
            new SqlParameter("@Score",dbnullFix(tables.Score)) ,
            new SqlParameter("@StoreAddress",dbnullFix(tables.StoreAddress)) ,
            new SqlParameter("@StoreName",dbnullFix(tables.StoreName)) ,
            new SqlParameter("@WaveName",dbnullFix(tables.WaveName)) ,
            new SqlParameter("@YearMonth",dbnullFix(tables.YearMonth)) ,
            new SqlParameter("@Quarter",dbnullFix(tables.Quarter)) ,
            new SqlParameter("@shopID",dbnullFix(tables.shopID)) ,
            new SqlParameter("@SessionName",dbnullFix(tables.SessionName)) 
            };
            var result = SqlHelper.ExecuteNonQuery(Connection, CommandType.Text, commandText, paramters);
            message.IsSuccess = result > 0;
            message.Message = string.Format("tb_intel_data_XOEM:更新了{0}条数据", result);
            return Json(message);
        }
    }
}
