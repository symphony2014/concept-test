import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import 'rxjs/add/operator/toPromise';
import { URL, URLSearchParams } from '@lvchengbin/url';
import { environment } from '../../../environments/environment';
@Injectable()
export class HttpService {
 public  get baseUrl(){
   return environment.api;
   // return (document.getElementById("api") as any).content;
 }
 getGroupUrl=this.baseUrl+"/api/external/getgroups";
 getExProjects=this.baseUrl+"/api/external/GetConceptProject";
 getModules=this.baseUrl+"/api/project/getmoduleorder";
 getzipUrl=this.baseUrl+"/api/upload/postDownloadZip";
 //getzipUrl="http://localhost:39607"+"/api/upload/postDownloadZip";
 getUserInfo=this.baseUrl+"/concept/getuserInfo";
 getCardSum=this.baseUrl+"/api/card/GetCardSum";
 getUsersUrl=this.baseUrl+"/api/external/getUsers";
 getTopicsUrl=this.baseUrl+"/api/external/getTopics";
 getMessagesUrl=this.baseUrl+"/api/ai/gettopicinfo";
 getWordCloudsUrl=this.baseUrl+"/api/ai/getwordclouddata";
 getautoIdUrl=this.baseUrl+"/api/report/getautoId";
 externalProjectsUrl=this.baseUrl+"/api/upload/GetExternalProjects";
 uploadUrl=this.baseUrl+"/api/Upload/UploadExcel";
 appendUrl=this.baseUrl+"/api/Upload/appendExcel";
  constructor(private http: Http) { }
  request(query: {url: string, method: string}, param: any,isExternal:boolean=false): Promise <any>{
    // let form = new URLSearchParams();
    // for(let key in param) {
    //   form.set(key,param[key]);
    // }
  
    const headers = new Headers();
    //headers.append('Content-Type', 'application/x-www-form-urlencoded');
    headers.set('Authorization', sessionStorage.getItem("token"))
    var url=isExternal?this.baseUrl:this.baseUrl;
    switch (query.method) {
      case 'post':
        return this.http.post(url+query.url, param,{headers:headers}).toPromise();
      case 'get':
      default:
        return this.http.get(url+query.url, {search: param,headers:headers}).toPromise();
    }
  }
}
