
import { Component, EventEmitter, OnInit } from '@angular/core';
import { UploadOutput, UploadInput, UploadFile, humanizeBytes, UploaderOptions } from 'ngx-uploader';
import { HttpService } from '../../services/http/http.service';
import { DashboardService } from '../dashboard/dashboard.service';
import { ActivatedRoute } from '@angular/router';
import { RequestOptions,Headers, Http } from "@angular/http";
import {Observable} from  "rxjs";
@Component({
  selector: 'appenduser',
  templateUrl: './appenduser.component.html',
  styleUrls: ['./appenduser.component.scss']
})
export class AppenduserComponent implements OnInit {

  

  options: UploaderOptions;
  formData: FormData;
  files: UploadFile[];
  uploadInput: EventEmitter<UploadInput>;
  humanizeBytes: Function;
  dragOver: boolean;

  projects:any;
  selectedPjId:string;
  constructor(private http:HttpService,private baseHttp:Http,private activatedRoute:ActivatedRoute,private dbs:DashboardService) {
    this.files = []; // local uploading files array
    this.uploadInput = new EventEmitter<UploadInput>(); // input events, we use this to emit data to ngx-uploader
    this.humanizeBytes = humanizeBytes;
    
  }
  
  ngOnInit() {
    this.baseHttp.get(this.http.externalProjectsUrl, {search: {nowpage:1,pagesize:10000}}).subscribe(data=>{
      this.projects=data.json().data.rows;
    });
    this.activatedRoute.params
                     .subscribe(param=>{
                       this.selectedPjId=this.dbs.transToObj(param).externalpjid
                      }); 
   
  }
 
  fileChange(event) {
    let fileList: FileList = event.target.files;
    if(fileList.length > 0) {
        let file: File = fileList[0];
        let formData:FormData = new FormData();
        formData.append('uploadFile', file, file.name);
        let headers = new Headers();
        /** No need to include Content-Type in Angular 4 */
        //headers.append('Content-Type', 'multipart/form-data');
        headers.append("Method","post");
        headers.append('Accept', 'application/json');
        let options = new RequestOptions({ headers: headers });
        this.baseHttp.post(`${this.http.appendUrl}?externalPjId=${this.selectedPjId}`, formData, options)
            .map(res => res.json())
            .subscribe(
                data => {
                  alert('success')
                  location.reload();
                },
                error => alert(error)
            )
    }
}
}