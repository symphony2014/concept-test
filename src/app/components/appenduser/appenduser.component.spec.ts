import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AppenduserComponent } from './appenduser.component';

describe('UploadComponent', () => {
  let component: AppenduserComponent;
  let fixture: ComponentFixture<AppenduserComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AppenduserComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AppenduserComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
