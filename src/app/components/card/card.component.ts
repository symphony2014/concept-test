import { Component, OnInit, Input, OnChanges, SimpleChanges } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { HttpService } from '../../services/http/http.service';
import { Http } from '@angular/http';

@Component({
  selector: 'app-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.scss']
})
export class CardComponent implements OnChanges {

    @Input("cardId")  cardId:string;
    @Input("externalPjId")  externalPjId:string;
    @Input("cardInfo")  cardInfo:any;
    public imgPreview:boolean;
    public showImgPreview:boolean=false;
    public cardChart:any;
    public dataDisabled:boolean=false;
    public chartDisabled:boolean=false;
    public tabSelected2:number;
    public options= {
     
        
        legend: {
            data:[]
        },
        xAxis: [
            {
              type: 'category',
              data: ['1月','2月','3月','4月','5月'],
              axisPointer: {
                  type: 'shadow'
                },
            }
        ],
        yAxis: [
            {
                type: 'value',
                name: '',
                
                
                axisLabel: {
                    formatter: '{value}'
                }
            },
            {
                type: 'value',
                name: '',
                                            axisLabel:{
                                                formatter:function(val){
                                                    return (val)+"%";
                                                }
                                            }
                
            }
        ],
        series: [
            
            {
                label:{
                    show:true
                },
                name:'',
                type:'bar',
                barWidth:"20px",
                color:"#30a451",
                data:[1,2,3,4,5],
            },
            {
             
                name:'', 
                type:'line',
                color:"#00c2de",
                data:[11,12,13,14,15],
                yAxisIndex:1,
                label:{
                    show:true,
                    formatter:function(val){
                        return Number(val.value).toFixed(0)+"%";
                }
            }
        },
      ]
    };
    
  constructor( private http:HttpService,private httpBase:Http) { }
  ngOnChanges(changes: SimpleChanges): void {
      this.tabSelected2=0;
      this.httpBase.get(this.http.getCardSum,{search:{cardId:this.cardId,externalPjId:this.externalPjId}})
      .subscribe(data=>{
          this.cardChart=data.json().data;
          //filter answer
          if(this.cardChart){
              this.cardChart.answers=this.cardChart.answers.filter(a=>a.Name!="");
              this.chartDisabled=  this.cardChart.answers.every(a=>a.Scores.length==0);
            }
        });
  }
  Number(val){
      return Number(val);
  }
  split(val){
      if(val.indexOf("|")>-1)
        return val.split('|')[0]
        else{
            return val;
        }
  }
  split2(val){
    if(val.indexOf("|")>-1)
      return val.split('|')[1]
      else{
          return val;
      }
}
mapToChart(rowData){
    if(rowData){
        this.options.xAxis[0].data=rowData.map(r=>r.Score+"分");
        this.options.series[0].data=rowData.map(r=>r.Count);
        this.options.series[1].data=rowData.map(r=>r.Percent);
        return this.options;
    }
}
open(msg){
  if(!msg.content && !msg.url){
    this.imgPreview=msg;
    this.showImgPreview=true;
  }else if(msg.content.indexOf("file")==0 || msg.content.indexOf("audio")==0)
 {
    window.open(msg.url);
  return;  
 }else {
   this.imgPreview=msg.url;
   this.showImgPreview=true;
  }
}
handleCancelImg(event){
  this.showImgPreview=false;
}
}
