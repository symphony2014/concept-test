import { Component, OnInit } from '@angular/core';
import { NzModalService } from 'ng-zorro-antd';
import {NzMessageService} from 'ng-zorro-antd';
import {HttpService} from '../../services/http/http.service';
import { ActivatedRoute} from '@angular/router';
@Component({
  selector: 'app-forum-group',
  templateUrl: './forum-group.component.html',
  styleUrls: ['./forum-group.component.scss']
})
export class ForumGroupComponent implements OnInit {
  isVisible = false;
  isConfirmLoading=true;
  groupList:Array<any>=[];
  group={
    GroupId:null,
    GroupName:'',
    CardOrder:'',
    ProjectAutoId:null,
    GroupPersonCount:0,
  }
  CardOrder:''
  constructor(
    private confirmServ: NzModalService,
    private route: ActivatedRoute,
    private http: HttpService,
    private _message: NzMessageService
  ) { }
  /**
   * 删除概念卡
   */
  showConfirm = (group) => {
    console.log(group)
    let that=this;
    this.confirmServ.confirm({
      title  : '您是否确认要删除这项内容',
      content: '<b>删除的内容将不再显示</b>',
      onOk() {
        console.log('确定');
        that.http.request({url:`/api/Group/DelGroup?groupId=${group.GroupId}`,method:'get'},null).then(data=>{
          data=JSON.parse(data._body)
          if(data.Result==0){
            that._message.success('成功', {nzDuration: 2000});
            that.getGroupList();
          }
        })
      }
    });
  }
  /**
 * 获取座谈会组列表
 * @type {ForumGroupComponent}
 */
  ngOnInit() {
    this.group.ProjectAutoId=this.route.snapshot.params['id'];
    this.getGroupList();
    this.getDefaultOrder();
  }
  handleCancel(){
    this.isVisible=false;
  }
  showEditGroup(group){
    this.isVisible=true;
    if(group){
      this.group.GroupName=group.GroupName;
      this.group.CardOrder=group.CardOrder;
      this.group.GroupPersonCount=group.GroupPersonCount;
      this.group.GroupId=group.GroupId;
    }
    else{
      this.group.GroupName='';
      this.group.CardOrder=this.CardOrder;
      this.group.GroupPersonCount=0
      this.group.GroupId=null;
    }
  }

  /**
   * 座谈会组列表
   */
  getGroupList(){
    let that=this;
    this.http.request({url:`/api/Group/GetGroupList?projectAutoId=${this.group.ProjectAutoId}`,method:'get'},null).then(data=>{
      data=JSON.parse(data._body)
      if(data.Result==0){
        that.groupList=data.data;
      }
    })
  }
  /**
   * 添加、编辑座谈会组
   * @param e
   */
  handleOk = () => {
    let that=this;
    this.http.request({url:`/api/Group/AddOrUpdateGroup`,method:'post'},this.group).then(data=>{
      data=JSON.parse(data._body)
      if(data.Result==0){
        that._message.success('成功', {nzDuration: 2000});
        that.isVisible = false;
        that.getGroupList();
      } else{
        that.isVisible = false;
        that._message.warning(data.Message, {nzDuration: 2000});
      }
    })
  }
  getDefaultOrder(){
    let that=this;
    this.http.request({url:`/api/ConceptCard/GetConceptCardDefaultOrder?projectAutoId=${this.group.ProjectAutoId}`,method:'get'},null).then(data=>{
      data=JSON.parse(data._body)
      console.log(data)
      that.CardOrder=data;
      console.log(that.group)
    })
  }
}
