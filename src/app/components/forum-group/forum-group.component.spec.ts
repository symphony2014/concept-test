import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ForumGroupComponent } from './forum-group.component';

describe('ForumGroupComponent', () => {
  let component: ForumGroupComponent;
  let fixture: ComponentFixture<ForumGroupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ForumGroupComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ForumGroupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
