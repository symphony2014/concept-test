import { Component, OnInit } from '@angular/core';
import {HttpService} from '../../services/http/http.service';
import { ActivatedRoute} from '@angular/router';
@Component({
  selector: 'app-report-detail',
  templateUrl: './report-detail.component.html',
  styleUrls: ['./report-detail.component.scss']
})
export class ReportDetailComponent implements OnInit {
  isVisible = false;
  ProjectAutoId:0;
  tabSelected:number=0;
  scoreReport={
    cards:[],
    header:[],
    groups:[]
  };
  orderReport={
    header:[],
    groups:[]
  };
  classifyReport={
    header:[],
    groups:[]
  };
  paintingReport={
    header:[],
    groups:[]
  };
  baseUrl:any;
  card:any;
  constructor(private http: HttpService, private route: ActivatedRoute){}
  ngOnInit() {
    this.baseUrl=this.http.baseUrl;
    this.ProjectAutoId=this.route.snapshot.params['id'];
    this.getGradingReport();
    this.getOrderReport();
    this.getClassifyReport();
    this.getPaintingReport();
  }
  showModal(card){
    this.isVisible = true;
    this.card=card;
  }
  handleOk = (e) => {
    this.isVisible = false;
  }
  handleCancel = (e) => {
    this.isVisible = false;
  }
  getGradingReport(){
    let that=this;
    this.http.request({url:`/api/Report/GetGradingIconReport?projectAutoId=${this.ProjectAutoId}`,method:'get'},null).then(data=>{
      data=JSON.parse(data._body)
      console.log(data)
      that.scoreReport=data;
    })
  }
  getOrderReport(){
    let that=this;
    this.http.request({url:`/api/Report/GetOrderReport?projectAutoId=${this.ProjectAutoId}`,method:'get'},null).then(data=>{
      data=JSON.parse(data._body)
      that.orderReport=data;
    })
  }
  getClassifyReport(){
    let that=this;
    this.http.request({url:`/api/Report/GetClassifyReport?projectAutoId=${this.ProjectAutoId}`,method:'get'},null).then(data=>{
      data=JSON.parse(data._body)
      that.classifyReport=data;
      console.log(data);
    })
  }
  getPaintingReport(){
    let that=this;
    this.http.request({url:`/api/Report/GetPaintingReport?projectAutoId=${this.ProjectAutoId}`,method:'get'},null).then(data=>{
      data=JSON.parse(data._body)
      console.log(data)
      that.paintingReport=data;
    })
  }
}
