import { Component, OnInit ,ChangeDetectorRef} from '@angular/core';
import * as d3 from 'd3'
import { HttpService } from '../../services/http/http.service';
import { ActivatedRoute, Params } from '@angular/router';
import { Http } from '@angular/http';
import domtoimage from 'dom-to-image';
import { unescape } from 'querystring';

@Component({
  selector: 'app-wordcloud',
  templateUrl: './wordcloud.component.html',
  styleUrls: ['./wordcloud.component.scss']
})
export class WordcloudComponent implements OnInit {
 

  constructor(private detect:ChangeDetectorRef, private http:HttpService,private angularHttp:Http,private activatedRoute: ActivatedRoute) { }
  _allChecked = false;
  _disabledButton = true;
  _checkedNumber = 0;
  _displayData: Array<any> = [];
  _operating = false;
  _dataSet = [];
  _indeterminate = false;
  _searchParms={};
  showWordClouds=false;
  _displayDataChange($event) {
    this._displayData = $event;
  }





  _operateData() {
    this.angularHttp.get(this.http.baseUrl+"/concept/GetTopicInfoExcel",{search:this._searchParms}).subscribe();
  }
toImage(){
  var node = document.getElementById('container');
domtoimage.toPng(node)
    .then(function (dataUrl) {
        var img = new Image();
        img.src = dataUrl;
        document.body.appendChild(img);
    })
    .catch(function (error) {
        console.error('oops, something went wrong!', error);
    });
    
}
 importSVG(sourceSVG, targetCanvas) {
  // https://developer.mozilla.org/en/XMLSerializer
  var svg_xml = (new XMLSerializer()).serializeToString(sourceSVG);
  var ctx = targetCanvas.getContext('2d');

  // this is just a JavaScript (HTML) image
  var img = new Image();
  // http://en.wikipedia.org/wiki/SVG#Native_support
  // https://developer.mozilla.org/en/DOM/window.btoa
  var result=btoa(unescape(encodeURIComponent(svg_xml)))
  //img.src = "data:image/svg+xml;base64," + ;

  img.onload = function() {
      // after this, Canvas’ origin-clean is DIRTY
      ctx.drawImage(img, 0, 0);
  }
}
  ngOnInit() {
     this.activatedRoute.params.subscribe((params: Params) => {
    //  let pjId= params['externalPjId'];
    //  let groupId= params['groupId'];
    //  let topicId= params['topicId'];
    //  let type= params['type'];
     for(var key in params){
       this._searchParms[key]=params[key];
     }
    // this._searchParms={pjId:pjId,groupId:groupId,topicId:topicId};

        this._buildWordClouds();

   });
}


  _buildWordClouds(): any {
    this.angularHttp.get(this.http.baseUrl+`/api/ai/getwordclouddata`,{search:this._searchParms}).subscribe(data=>{
 
      var result=data.json();
       
      if(!result.data){
        alert("数据还没有计算完成！");
        return;
      }
      result.data.sort((a,b)=>a.TopicId-b.TopicId);
      result.data.forEach((topic,i)=> {
     var svgelement= document.createElementNS("http://www.w3.org/2000/svg","svg");

        svgelement.setAttributeNS(null,"width","500");
        svgelement.setAttributeNS(null,"height","500");
        svgelement.setAttributeNS(null,"id","topic"+i);
     document.getElementById("container").appendChild(svgelement);
    var svg = d3.select("#topic"+i)
    // width = +svg.attr("width"),
    // height = +svg.attr("height");
    svg.attr("width",500);
    svg.attr("height",500);
    svg.attr("text-anchor","middle");
    svg.attr("font-family","sans-serif");
    svg.attr("font-size",10);
     var width=500;
     var height=500; 
var format = d3.format(",d");

var color = d3.scaleOrdinal(d3.schemeCategory20c);

var pack = d3.pack()
    .size([width, height])
    .padding(1.5);

  var ndata=JSON.parse(topic.JSON).ids;
  ndata.columns=["id","value"];
  var root = d3.hierarchy({children: ndata})
      .sum(function(d) { return d.value; })
      .each(function(d) {
        if (id = d.data.id) {
          var id, i = id.lastIndexOf(".");
          d.id = id;
          d.package = id.slice(0, i);
          d.class = id.slice(i + 1);
        }
      });

  var node = svg.selectAll(".node")
    .data(pack(root).leaves())
    .enter().append("g")
      .attr("transform", function(d) { return "translate(" + d.x + "," + d.y + ")"; });

  node.append("circle")
      .attr("id", function(d) { return d.id; })
      .attr("r", function(d) { return d.r; })
      .style("fill", function(d) { return color(d.package); });

  node.append("clipPath")
      .attr("id", function(d) { return "clip-" + d.id; })
    .append("use")
      .attr("xlink:href", function(d) { return "#" + d.id; });
  node.append("text")
      .attr("clip-path", function(d) { return "url(#clip-" + d.id + ")"; })
    .selectAll("tspan")
    .data(function(d) { 
      var words=decodeURI(d.class);
      if(words.length>10){
        return words.match(/.{8}/g);
      }
   return [decodeURI(d.class)];
    })
    .enter().append("tspan")
      .attr("x", 0)
      .attr("y", function(d, i, nodes) { return 13 + (i - nodes.length / 2 - 0.5) * 10; })
      .text(function(d) {
         return d;
         });

  node.append("title")
      .text(function(d) { 
        return decodeURI(d.id) + "\n" + format(d.value);
       });
});
      });

  }
}
