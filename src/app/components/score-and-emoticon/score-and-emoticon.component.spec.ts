import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ScoreAndEmoticonComponent } from './score-and-emoticon.component';

describe('ScoreAndEmoticonComponent', () => {
  let component: ScoreAndEmoticonComponent;
  let fixture: ComponentFixture<ScoreAndEmoticonComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ScoreAndEmoticonComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ScoreAndEmoticonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
