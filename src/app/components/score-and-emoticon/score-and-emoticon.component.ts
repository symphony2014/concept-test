import { Component, OnInit } from '@angular/core';
import {NzMessageService} from 'ng-zorro-antd';
import {HttpService} from '../../services/http/http.service';
import { ActivatedRoute} from '@angular/router';
@Component({
  selector: 'app-score-and-emoticon',
  templateUrl: './score-and-emoticon.component.html',
  styleUrls: ['./score-and-emoticon.component.scss']
})
export class ScoreAndEmoticonComponent implements OnInit {
  grade={
    ProjectAutoId:0,
    IsShowGrading:true,
    IsShowOpenQuestion:true,
    OpenQuestion:'',
    GradingQuestionList:[]
  };
  emotion={
    ProjectAutoId:0,
    IsShowEmoticon:false,
    EmoticonDesc:'',
    EmoticonQuestionOptionList:[]
  };
  _console(value) {
    console.log(value);
  }
  constructor(
    private http: HttpService,
    private _message: NzMessageService,
    private route: ActivatedRoute,
  ) { }
  ngOnInit() {
    this.grade.ProjectAutoId=this.route.snapshot.params['id'];
    this.emotion.ProjectAutoId=this.route.snapshot.params['id'];
    this.getGrad();
    this.getEmotion();
  }
  getGrad(){
    let that=this;
    this.http.request({url:`/api/GradingIcon/GetGradingSetting?projectAutoId=${this.grade.ProjectAutoId}`,method:'get'},null).then(data=>{
      data=JSON.parse(data._body)
      if(data.Result==0){
        console.log(data.data)
        that.grade.IsShowGrading=data.data.IsShowGrading==0?false:true;
        that.grade=JSON.parse(JSON.stringify(data.data));
      }
    })
  }
  getEmotion(){
    let that=this;
    this.http.request({url:`/api/GradingIcon/GetEmoticonSetting?projectAutoId=${this.emotion.ProjectAutoId}`,method:'get'},null).then(data=>{
      data=JSON.parse(data._body)
      if(data.Result==0){
        console.log(data.data)
        //that.emotion.IsShowGrading=data.data.IsShowGrading==0?false:true;
        that.emotion=JSON.parse(JSON.stringify(data.data));
      }
    })
  }
  setGrad(){
    let that=this;
    this.http.request({url:`/api/GradingIcon/UpdateGradingSetting`,method:'post'},this.grade).then(data=>{
      data=JSON.parse(data._body)
      if(data.Result==0){
        that._message.success('成功', {nzDuration: 2000});
      }else{
        that._message.warning(data.Message, {nzDuration: 2000});
      }
    })
  }
  setEmotion(){
    let that=this;
    this.http.request({url:`/api/GradingIcon/UpdateEmoticonSetting`,method:'post'},this.emotion).then(data=>{
      data=JSON.parse(data._body)
      if(data.Result==0){
        that._message.success('成功', {nzDuration: 2000});
      }else{
        that._message.warning(data.Message, {nzDuration: 2000});
      }
    })
  }
}
