import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConfigSortComponent } from './config-sort.component';

describe('ConfigSortComponent', () => {
  let component: ConfigSortComponent;
  let fixture: ComponentFixture<ConfigSortComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConfigSortComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConfigSortComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
