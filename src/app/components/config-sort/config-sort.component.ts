import { Component, OnInit } from '@angular/core';
import { NzModalService } from 'ng-zorro-antd';
import {NzMessageService} from 'ng-zorro-antd';
import {HttpService} from '../../services/http/http.service';
import { ActivatedRoute} from '@angular/router';
@Component({
  selector: 'app-config-sort',
  templateUrl: './config-sort.component.html',
  styleUrls: ['./config-sort.component.sass']
})
export class ConfigSortComponent implements OnInit {
  orderQuestion={
    OrderQuestionId:null,
    ProjectAutoId:null,
    OrderQuestionText:''
  }
  orderQuestionList:Array<any>=[];
  constructor(
    private route: ActivatedRoute,
    private http: HttpService,
    private _message: NzMessageService
  ) { }

  ngOnInit() {
    this.orderQuestion.ProjectAutoId=this.route.snapshot.params['id'];
    let that=this;
    this.http.request({url:`/api/Order/GetOrderQuestion?projectAutoId=${this.orderQuestion.ProjectAutoId}`,method:'get'},null).then(data=>{
      data=JSON.parse(data._body)
      if(data.Result==0){
        console.log(data.data)
        that.orderQuestionList=data.data;
      }
      else{
        that._message.warning("some errs occur", {nzDuration: 2000});
      }
    })
  }
  updateQuestion(){
    let that=this;
    console.log(this.orderQuestionList)
    this.http.request({url:`/api/Order/UpdateOrderQuestion`,method:'post'},this.orderQuestionList).then(data=>{
      data=JSON.parse(data._body)
      if(data.Result==0){
        that._message.success('成功', {nzDuration: 2000});
      } else{
        that._message.warning(data.Message, {nzDuration: 2000});
      }
    })
  }
}
