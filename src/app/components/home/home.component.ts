import { Component, OnInit } from '@angular/core';
import { NzModalService } from 'ng-zorro-antd';
import {HttpService} from '../../services/http/http.service';
import {NzMessageService} from 'ng-zorro-antd';
@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  constructor(private confirmServ: NzModalService,private http: HttpService, private _message: NzMessageService) { }
  isVisible = false;
  projectList:any=[];
  isConfirmLoading = false;
  project={
    ProjectId:null,
    ProjectName:'',
    ProjectDesc:'',
    ProjectAutoId:null,
    IsNeedDoodled:1,
    ModuleOrder:''
  }
  TotalCount:number
  page:number
  showModal = (data) => {
    this.isVisible = true;
    if(data){
      this.project.ProjectId=data.ProjectId;
      this.project.ProjectName=data.ProjectName;
      this.project.ProjectDesc=data.ProjectDesc;
      this.project.ProjectAutoId=data.ProjectAutoId;
      this.project.ModuleOrder=data.ModuleOrder;
      this.project.IsNeedDoodled=data.IsNeedDoodled;
    }
    else{
      this.project.ProjectId=null;
      this.project.ProjectName='';
      this.project.ProjectDesc='';
      this.project.ProjectAutoId='';
    }
  }
  /**
   * 添加或更新
   * @param e
   */
  handleOk = (e) => {
    let that=this;
    this.isConfirmLoading = true;
    this.http.request({url:`/api/Project/AddOrUpdateProject`,method:'post'},this.project).then(function(data){
      that.isConfirmLoading = false;
      that.isVisible = false;
      data=(JSON.parse(data._body))
      if(data.Result==0){
        that._message.success('成功', {nzDuration: 2000});
        console.log(data)
        that.toPage(that.page)
      }
      else that._message.error('失败', {nzDuration: 2000});
    })
  }

  handleCancel = (e) => {
    this.isVisible = false;
  }

  /**
   * 删除项目
   * @param id
   */
  showConfirm = (id) => {
    let that=this;
    this.confirmServ.confirm({
      title  : '您是否确认要删除这项内容',
      content: '<b>删除的内容将不再显示</b>',
      onOk() {
          that.http.request({url:`/api/Project/DelProject?projectAutoId=${id}`,method:'get'},null).then(data=>{
            data=(JSON.parse(data._body))
            if(data.Result==0){
              that._message.success('删除成功', {nzDuration: 2000});
              that.toPage(that.page)
            }
            else that._message.error('失败了', {nzDuration: 2000});
          })
      },
      onCancel() {
      }
    });
  }
  ngOnInit() {
    this.project.ProjectAutoId=
    this.toPage(1)
  }
  toPage(e) {
    let that=this;
    this.page=e;
    this.http.request({url:`/api/Project/GetProjectList?pageIndex=${e}&pageSize=10`,method:'get'},null).then((data)=>{
      data=JSON.parse(data._body)
      console.log(data)
      if(data.Result==0){
        that.TotalCount=data.TotalCount
        that.projectList=data.data
      }
    })
  }
}
