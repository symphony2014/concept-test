import { Component, OnInit ,ChangeDetectorRef, Injectable} from '@angular/core';
import * as d3 from 'd3'
import { HttpService } from '../../services/http/http.service';
import { ActivatedRoute, Params } from '@angular/router';
import { Http } from '@angular/http';
import domtoimage from 'dom-to-image';
import { unescape } from 'querystring';


@Injectable()
export class DashboardService{
//   fackData:any={
//       comment:
//   };
  constructor(private http:Http,private http2:HttpService){}
  transToObj(obj):any{
    var newParams={};
    for(var key in obj){
     newParams[key.toLowerCase()]=obj[key];
   }
   return newParams;
  }
  buildwordcloud(domId,data){
 
    var dom= document.getElementById(domId)
    if(!dom){
      return ;
    }
    dom.innerHTML="";
     
     if(!data || data.length==0){
       return;
     }
       

      var  i=0;
     var svgelement= document.createElementNS("http://www.w3.org/2000/svg","svg");

        svgelement.setAttributeNS(null,"width","389");
        svgelement.setAttributeNS(null,"height","400");
        svgelement.setAttributeNS(null,"id","topic"+i);
        svgelement.setAttributeNS(null,"color","white");
        //clear dom
     document.getElementById(domId).appendChild(svgelement);
    var svg = d3.select("#topic"+i)
    // width = +svg.attr("width"),
    // height = +svg.attr("height");
    svg.attr("width",450);
    svg.attr("height",500);
    svg.attr("text-anchor","middle");
    svg.attr("font-family","sans-serif");
    svg.attr("font-size",10);
 
     var width=389;
     var height=600; 
var format = d3.format(",d");

var color = d3.scaleOrdinal(d3.schemeCategory20c);

var pack = d3.pack()
    .size([width, height])
    .padding(1.5);
 data=data.slice(0,15);
 data.columns=["id","value"];
  
  var root = d3.hierarchy({children: data})
      .sum(function(d) { return d.value; })
      .each(function(d) {
        if (id = d.data.id) {
          var id, i = id.lastIndexOf(".");
          d.id = id;
          d.package = id.slice(0, i);
          d.class = id.slice(i + 1);
        }
      });

  var node = svg.selectAll(".node")
    .data(pack(root).leaves())
    .enter().append("g")
      .attr("transform", function(d) { return "translate(" + d.x + "," + d.y + ")"; });

  node.append("circle")
      .attr("id", function(d) { return d.id; })
      .attr("r", function(d) { return d.r*1.4; })
      .style("fill", function(d) { return color(d.package); });

  node.append("clipPath")
      .attr("id", function(d) { return "clip-" + d.id; })
    .append("use")
      .attr("xlink:href", function(d) { return "#" + d.id; });
  node.append("text")
      .attr("clip-path", function(d) { return "url(#clip-" + d.id + ")"; })
      .attr("style","fill: white;font-size:12px")
    .selectAll("tspan")
    .data(function(d) { 
      var words=decodeURI(d.class);
      if(words.length>10){
        return words.match(/.{8}/g);
      }
   return [decodeURI(d.class)];
    })
    .enter().append("tspan")
      .attr("x", 0)
      .attr("y", function(d, i, nodes) { return 13 + (i - nodes.length / 2 - 0.5) * 10; })
      .text(function(d) {
         return d;
         });

  node.append("title")
      .text(function(d) { 
        return decodeURI(d.id) + "\n" + format(d.value);
       });

     // });

  }
}