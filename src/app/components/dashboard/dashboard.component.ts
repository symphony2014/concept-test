import { Component, OnInit,AfterViewInit, ChangeDetectorRef, ViewChild } from '@angular/core';
import {Http, Headers,RequestOptions} from "@angular/http";
import { HttpService } from '../../services/http/http.service';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/switchMap';
import 'rxjs/add/operator/withLatestFrom';
import 'rxjs/add/operator/combineLatest';
import 'rxjs/add/operator/filter';
import 'rxjs/add/operator/do';

import { DashboardService } from './dashboard.service';
//import domtoimage from 'dom-to-image';
import html2canvas from 'html2canvas'; 
import { SearchType } from './SearchType';
import { BehaviorSubject } from 'rxjs';
@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']

})
export class DashboardComponent implements OnInit,AfterViewInit {
  ngAfterViewInit(): void {
    //this.dashboardService.buildwordcloud("container",this.AIData.data.ids);
  }

  public groups:Observable<any>;
  public topics:Observable<any>;
  public exProjects:Observable<any>;
  public triggerByPjId:BehaviorSubject<any>=new BehaviorSubject<any>(null);
  public exModules:any;
  public messages:Observable<any>;
  public clusters:Observable<any>;
  public clustersSet:any[]=[];
  public scoreReport:any={groups:[]};
  public orderReport:any={groups:[]};
  public classifyReport:any={groups:[]};
  public paintingReport:any={groups:[]};
  public gradingtotalReport:any={groups:[]};
  public tabSelected2=0;
  public isSpinning3=false;
  public imgPreview="";
  public showImgPreview=false;
  public searchKey="";
  public searchKey2="";
  public AIerror="";
  public cardInfo;
  public searchType:Number=1;
  public tabSelected2_tmp;
  public urlMatchRegex="/(https?:\/\/(www\.)?[-a-zA-Z0-9@:%._\+~#=]{2,256}\.[a-z]{2,4}\b([-a-zA-Z0-9@:%_\+.~#?&//=]*))/g";
  _dataSet = [];
  public array=[1,2,3,4];
  _userOriginSet=[];
  _isSpinning=true;
  _isSpinning2=true;
  parms:any={};
  messagesSet=[];
  messagesOriginSet=[];
  isPeople=false;
  tempDetail:any={};
  AIData:any={data:{comments:[],clusters:[],ids:[]}};
  isIE=navigator.userAgent.indexOf('Trident')>0;
  projects:any=[];
  @ViewChild("project") project:any;
   routeUrl():any{
     var param=this.activatedRoute.snapshot.params;
   return this.dashboardService.transToObj(param);
  }
  constructor(private cd: ChangeDetectorRef,private http:Http,private httpService:HttpService,private activatedRoute: ActivatedRoute,private dashboardService:DashboardService) { }
  selectChange(){
    this._isSpinning=true;
    this.updateUsers(this.routeUrl().externalpjid,this.parms.groupid);
  }
  showPeopleDetail(data){
  this.isPeople=true;
  this.tempDetail=data;
  }
  handleCancel(event){
   this.isPeople=false; 
  }
  selectChange2(groupId:string){
    this.updateUsers(this.routeUrl().externalpjid,groupId);
  }
  search2(key,type){
     this.messagesSet=this.messagesOriginSet.filter(r=>r.msgCntnt)
                  .filter(r=>{
                    if(type==SearchType.Content){
                      if(r.msgCntnt.content){
                        return r.msgCntnt.content.indexOf(key.trim())>-1
                      }
                      if(r.msgCntnt){
                        return r.msgCntnt.indexOf(key.trim())>-1
                      }
                    }else{
                      return (r.username+r.nckNm).indexOf(key.trim())>-1
                    }
                  });
  }
  search(key){
     this._dataSet=this._userOriginSet.filter(r=>r.map.NickName || r.map.Name)
                  .filter(r=>{
                    if(r.map.NickName){
                      return r.map.NickName.indexOf(key.trim())>-1
                    }
                    if(r.map.Name){
                      return r.map.Name.indexOf(key.trim())>-1
                    }
                  });
  }
  changeTab(selected){
    console.log(selected);
  }
  updateUsers(externalPjId,groupId){
    this.http.get(this.httpService.getUsersUrl,{search:{externalPjId:externalPjId,groupId:groupId}})
             .subscribe(data=>{
               var json=data.json();
              this._dataSet=  json.data;
              this._userOriginSet=json.data;
              for(var i=0;i<this._userOriginSet.length;i++){
                  this._userOriginSet[i].pairs.splice(0,1);
              }
               this._isSpinning=false;
             });
  }
  reset(){
    this.parms.groupid="-1";
    this.parms.topicid="-1";
  }
  updateAll(parms){
        this._isSpinning2=true;
        
        this.updateMessages2(parms);
        this.updateWordClouds(parms);
        
  }
  updateWordClouds(params){
    this.http.get(this.httpService.getWordCloudsUrl,{search:params}).subscribe(data=>{
      this.AIData=data.json(); 
      if(!this.AIData.data){
        this.AIData.data={comments:[],clusters:[],ids:[]};
      }
      this.dashboardService.buildwordcloud("container",this.AIData.data.ids.sort(function(a,b){ return b.value-a.value}));
   });
  }
  updateMessages2(param){
     this.messages=this.http.get(this.httpService.getMessagesUrl,{search:param});
     this.messages.subscribe(data=>{
       this.messagesOriginSet=data.json();

       //change url
      this.messagesSet=this.messagesOriginSet=this.messagesOriginSet.map(message=>{
        var content=message.msgCntnt;
        var newcontent=message.msgCntnt;
        if(content.indexOf("http")>-1){
            //var url=this.urlMatchRegex.match(content);
            //var url=content.match(this.urlMatchRegex)[0];
            if(content.indexOf("audio")==0){
               var audiourl=content.split('[')[1].replace("]","");
              newcontent={content:content,url: audiourl,img:"/assets/imgs/audio.png"};
            }
            if(content.indexOf("img")==0){
              var imgurl=content.split('[')[1].replace("]","");
              
              newcontent={ content:content,url: imgurl,img:"/assets/imgs/url.png"};
            }
            if(content.indexOf("price")==0){
              var imgurl=content.split('|')[1].replace("]","");
              newcontent={content:content,url: imgurl,title:content.split('|')[2],img:imgurl};
            }
            if(content.indexOf("file")==0){
              //var fileurl=content.split('[')[1].replace("]","");
             var fileurl= content.match(/(\([^(]+\))/g)[0].replace("(","").replace(")","")
              
            
              newcontent={
                content:content,
                url: fileurl,
                fordownload:true,
                img:"/assets/imgs/file.png",
                title:content.split('[')[1].replace("]","")
              };
            }
        }

      message.msgCntnt=newcontent;
   return message;
      });
       this._isSpinning2=false;
     });
  }

  groupChange(param){
  this.parms.topicid="-1";
   this.topics=this.http.get(this.httpService.getTopicsUrl,{search:param})
                     .map(data =>data.json().data.data.rows)
                     .map(rows=>rows.filter(r=>r.sampGrpId==param.groupid));

  }
  getDem(header:any[]){
    if(!header || !header.length)return [];
    var length= header.length/2 
    var init=[];
    for (var i=0;i<length-1;i++)
    {
      init.push("维度"+(i+1));
    }
    return init;
  }
  isUrl(content){
    console.log(content)
    console.log(content.indexOf('http'))
    return content.indexOf('http')>-1;
  }
  handleCancelImg(event){
    this.showImgPreview=false;
  }
  downloadImage(){
    
    html2canvas(document.getElementById('container'))
    .then(function (dataUrl) {
        var link = document.createElement('a');
        link.download = 'word-clouds.jpg';
        link.href = dataUrl;
        link.click();
    });
  }
  adoptCount(rows){
    if(!rows){
      return [];
    }
      var newRow=[];
      for(var i=0;i<rows.length;i++){
        rows[i][0]={img:rows[i][0],text:rows[i][1]};
        (rows[i] as any[]).splice(1,1);
      }
      return rows;
  }
  ngOnInit() {
  
  var routeStream=this.activatedRoute.params.map(param=>this.dashboardService.transToObj(param)) ;
  this.groups=routeStream.switchMap(params=>this.http.get(this.httpService.getGroupUrl,{search:params}))
                         .map(data =>data.json().data.data.rows);

  this.exProjects=routeStream.switchMap(params=>this.http.get(this.httpService.getExProjects,{search:{choosedConceptPrjId:params.externalpjid}}))
                         .map(data =>data.json().data.data.rows)
                         .do(data=>{
                           if(data.length==0){
                             this.isSpinning3=false;
                           }
                         })
                         .filter(data=>data.length)
                         .map(data=>(data as any[]).filter(f=>f.mtrlSrccd=='Concept Test'))

                         routeStream.subscribe(params=>this.triggerByPjId.next(params.concepttestpjid));

                      this.triggerByPjId.filter(pjId=>pjId!=-1).subscribe(data=>{
                        this.http.get(this.httpService.getModules,{search:{prjId:data}})
                         .map(data=>data.json().data)
                         .map(data=>{
                           var arr=[];
                           for(var key in data){
                             arr.push({Key:key,Value:data[key]});
                           }
                           return arr;
                         })
                         .do(arr=>{
                           this.exModules=arr;
                           this.tabSelected2_tmp=arr[0].Key;
                           this.cd.markForCheck();
                         }).subscribe();
                      });
                 

  
  this.topics= routeStream.switchMap(params=>this.http.get(this.httpService.getTopicsUrl,{search:params}))
                    .withLatestFrom(routeStream)
                     .map(data=>{
                         var rowData=data[0].json().data.data.rows;
                         var result=rowData.filter(r=>r.sampGrpId==data[1].groupid);
                         return result;
                     })
                   

                     
                     //设置默认值
                     this.activatedRoute.params.subscribe(data=>{
                       this.parms=this.dashboardService.transToObj(data);
                     });
                     //人员报告
                     this.groups.subscribe(groups=>{
                       this.updateUsers(this.routeUrl().externalpjid,this.parms.groupid);
                     });
//定性报告-聊天信息
 this.activatedRoute.params
                    .subscribe(param=>{
                                 this.updateMessages2(this.dashboardService.transToObj(param));
                               });
  
   

  //定性报告-词云
  //this.dashboardService.buildwordcloud("container",this.routeUrl());
 

 //定性报告-ai data
  this.activatedRoute.params
  .map(param=>this.dashboardService.transToObj(param)) 
  .switchMap(params=>this.http.get(this.httpService.getWordCloudsUrl,{search:params}))
  .subscribe(data=>{
    this.AIData=data.json();
    if(this.AIData.data){
      this.dashboardService.buildwordcloud("container",this.AIData.data.ids)
    }
  });
 
  
  //定量报告
  this.isSpinning3=true;
  this.activatedRoute.params
  .map(param=>this.dashboardService.transToObj(param)) 
  .combineLatest(this.exProjects)
  .do(result=>{
    this.parms.choosedConceptPrjId_tmp=result[0].concepttestpjid;
    if(this.parms.concepttestpjid==-1){
      this.isSpinning3=false;
    }
  })
  .switchMap(result=>{

    return this.http.get(this.httpService.getautoIdUrl,{ search:{projectId:result[0].concepttestpjid ||result[1][0].qstnrId} }
)
})
  .combineLatest(this.exProjects)
  .subscribe(result=>{
    var data=result[0].json();
    var exFirstProjectId=result[1][0].qstnrId;
    var route=this.routeUrl(); 
    if(data && data.data){
      this.getCardInfo(data.data,route.externalpjid,route.groupid,this.tabSelected2_tmp);
      this.getOrderReport(data.data,route.externalpjid,route.groupid,exFirstProjectId);
      this.getClassifyReport(data.data,route.externalpjid,route.groupid,exFirstProjectId);
      this.getPaintingReport(data.data,route.externalpjid,route.groupid,exFirstProjectId);
      this.GetGradingTotalReport(data.data,route.externalpjid,route.groupid,exFirstProjectId);
    }else{
      this.isSpinning3=false;
    }
  })

  this.http.get(this.httpService.externalProjectsUrl, {search: {nowpage:1,pagesize:10000}}).subscribe(data=>{
    this.projects=data.json().data.rows;
  }); 
}
updateReport(param){
  this.parms.choosedConceptPrjId=this.parms.choosedConceptPrjId_tmp;
  this.tabSelected2=this.tabSelected2_tmp;
this.isSpinning3=true; 
this.http.get(this.httpService.getautoIdUrl,{
    search:{projectId:param.choosedConceptPrjId}})
  .subscribe(result=>{
    var data=result.json();
    
    if(data && data.data){
      
      this.getCardInfo(data.data,param.externalpjid,param.groupid,this.tabSelected2);
      this.getOrderReport(data.data,param.externalpjid,param.groupid,param.choosedConceptPrjId);
      this.getClassifyReport(data.data,param.externalpjid,param.groupid,param.choosedConceptPrjId);
      this.getPaintingReport(data.data,param.externalpjid,param.groupid,param.choosedConceptPrjId);
      this.GetGradingTotalReport(data.data,param.externalpjid,param.groupid,param.choosedConceptPrjId);
    }
  })
}
getCardInfo(cpjid,externalpjid,groupid,cardId){
  this.http.get(this.httpService.baseUrl+`/api/Report/GetGradingIconReport2?projectAutoId=${cpjid}&externalpjid=${externalpjid}&groupid=${groupid}&cardId=${cardId}`,null)
  .combineLatest(this.http.get(this.httpService.baseUrl+`/api/Report/GetPaintingReport?projectAutoId=${cpjid}&externalpjid=${externalpjid}&groupid=${groupid}&cardId=${cardId}`,null))
  .map(result=>{
      var data1=result[1].json();
      var data2=result[0].json();
      //merge header
      data2.header.splice(1,0,"涂鸦");
      for(var j=0;j<data2.groups.length;j++)
      {
        var group=data2.groups[j];
         for(var i=0;i<group.device.length;i++){
           if(data1.groups[j].device[i])
           group.device[i+2].cards=data1.groups[j].device[i].cards;
         }
      }
      
      return data2;
  })
  .subscribe(data=>{
    this.cardInfo=data;
    this.tabSelected2=this.tabSelected2_tmp;
    this.isSpinning3=false;
  });
  ;
  
}
getOrderReport(cpjid,externalpjid,groupid,choosedConceptPrjId){
  this.http.get(this.httpService.baseUrl+`/api/Report/GetOrderReport2?projectAutoId=${cpjid}&externalpjid=${externalpjid}&groupid=${groupid}&choosedConceptPrjId=${choosedConceptPrjId}`,null).subscribe(data=>{
    this.orderReport=data.json();
  })
}
getClassifyReport(cpjid,externalpjid,groupid,choosedConceptPrjId){

  this.http.get(this.httpService.baseUrl+`/api/Report/GetClassifyReport2?projectAutoId=${cpjid}&externalpjid=${externalpjid}&groupid=${groupid}&choosedConceptPrjId=${choosedConceptPrjId}`,null).subscribe(data=>{
    this.classifyReport=data.json();
  })

}
getPaintingReport(cpjid,externalpjid,groupid,choosedConceptPrjId){
  this.http.get(this.httpService.baseUrl+`/api/Report/GetPaintingReport?projectAutoId=${cpjid}&externalpjid=${externalpjid}&groupid=${groupid}&choosedConceptPrjId=${choosedConceptPrjId}`,null).subscribe(data=>{
    this.paintingReport=data.json();
  })
}
projectChange(prjId){
 this.triggerByPjId.next(this.project._value);
}
GetGradingTotalReport(cpjid,externalpjid,groupid,choosedConceptPrjId){
  this.http.get(this.httpService.baseUrl+`/api/Report/GetGradingTotalReport?projectAutoId=${cpjid}&externalpjid=${externalpjid}&groupid=${groupid}&choosedConceptPrjId=${choosedConceptPrjId}`,null).subscribe(data=>{
    this.gradingtotalReport=data.json();

    this.gradingtotalReport.Contents=this.adoptCount(this.gradingtotalReport.Contents);
  })
}
downloadZip(param){

        this._isSpinning2=true;
        let headers = new Headers();
        /** No need to include Content-Type in Angular 4 */
        //headers.append('Content-Type', 'multipart/form-data');
        headers.append("Method","post");
        headers.append('Accept', 'application/json');
        headers.append('Content-Type', 'application/json');
        let options = new RequestOptions({ headers: headers });

    html2canvas(document.getElementById('container'))
     .then((canvas:HTMLCanvasElement)=>{
       this.http.post(this.httpService.getzipUrl,Object.assign({dataUrl:canvas.toDataURL()},this.parms)).subscribe(res=>{

         this._isSpinning2=false;

         location.href=this.httpService.baseUrl+"/"+ res.json();
       });
      })
   
}
open(msg){
  if(!msg.content && !msg.url){
    this.imgPreview=msg;
    this.showImgPreview=true;
  }else if(msg.content.indexOf("file")==0 || msg.content.indexOf("audio")==0)
 {
    window.open(msg.url);
  return;  
 }else {
   this.imgPreview=msg.url;
   this.showImgPreview=true;
  }
}
}
