import { Component, OnInit } from '@angular/core';
import {HttpService} from '../../services/http/http.service';
import 'rxjs/add/operator/toPromise';
import {NzMessageService} from 'ng-zorro-antd';
declare var $:any;
import {
  FormBuilder,
  FormGroup,
  Validators
} from '@angular/forms';
import {Router} from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  public validateForm: FormGroup;
  constructor(private fb: FormBuilder, private router: Router,private http: HttpService,private _message: NzMessageService) {
  }
  submitForm() {
    /*for (const i in this.validateForm.controls) {
      this.validateForm.controls[ i ].markAsDirty();
    }*/
  }
  ngOnInit() {
    this.validateForm = this.fb.group({
      username: [ null, [ Validators.required ] ],
      password: [ null, [ Validators.required ] ],
      grant_type:'password',
      remember:true
    });
  }
  login() {
    let that=this;
    $.ajax({
      type:'post',
      data:this.validateForm.value,
      url:this.http.baseUrl+'/token',
      success:function(data){
        if(data){
          sessionStorage.setItem("token",data.token_type+' '+data.access_token)
          sessionStorage.setItem("user",that.validateForm.value.username)
          that.router.navigate(['/home']);
        }
      },
      error:function (data) {
        console.log(data)
        that._message.warning("用户名密码不正确", {nzDuration: 2000});
      }
    })
  }
}
