import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConfigHeapComponent } from './config-heap.component';

describe('ConfigHeapComponent', () => {
  let component: ConfigHeapComponent;
  let fixture: ComponentFixture<ConfigHeapComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConfigHeapComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConfigHeapComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
