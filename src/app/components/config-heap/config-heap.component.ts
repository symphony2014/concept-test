import { Component, OnInit } from '@angular/core';
import { NzModalService } from 'ng-zorro-antd';
import {NzMessageService} from 'ng-zorro-antd';
import {HttpService} from '../../services/http/http.service';
import { ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-config-heap',
  templateUrl: './config-heap.component.html',
  styleUrls: ['./config-heap.component.sass']
})
export class ConfigHeapComponent implements OnInit {
  heap={
    ClassifyQuestionId:null,
    ProjectAutoId:null,
    ClassifyQuestionText:''
  }
  heapList:Array<any>=[]
  constructor(
    private route: ActivatedRoute,
    private http: HttpService,
    private _message: NzMessageService
  ) { }

  /**
   * 获取分堆题目设置
   */
  ngOnInit() {
    this.heap.ProjectAutoId=this.route.snapshot.params['id'];
    let that=this;
    this.http.request({url:`/api/Order/GetClassifyQuestion?projectAutoId=${this.heap.ProjectAutoId}`,method:'get'},null).then(data=>{
      data=JSON.parse(data._body)
      if(data.Result==0){
        that.heapList=data.data;
        console.log(data.data)
      }
    })
  }

  /**
   * 后台接口有问题
   */
  updateQuestion(){
    let that=this;
    this.http.request({url:`/api/Order/UpdateClassifyQuestion`,method:'post'},this.heapList).then(data=>{
      data=JSON.parse(data._body)
      if(data.Result==0){
        that._message.success('成功', {nzDuration: 2000});
      } else{
        that._message.warning(data.Message, {nzDuration: 2000});
      }
    })
  }
}
