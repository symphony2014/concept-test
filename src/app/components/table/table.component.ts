import { Component, OnInit } from '@angular/core';
import { Http } from '@angular/http';
import { HttpService } from '../../services/http/http.service';
import { ActivatedRoute, Params } from '@angular/router';


@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.scss']
})
export class TableComponent implements OnInit {
  _dataSet = [];
  _searchParms={};
  constructor(private activatedRoute:ActivatedRoute,private http:Http,private httpBase:HttpService) { }

  ngOnInit() {
    this.activatedRoute.params.subscribe((params: Params) => {
      for(var key in params){
        this._searchParms[key]=params[key];
      }

         this._buildTable();

    });
  }
  _buildTable(){
    this.http
    .get(this.httpBase.baseUrl+`/api/ai/gettopicInfo`,{search:this._searchParms})
    .subscribe(data=>{
       this._dataSet=data.json();
       if(this._dataSet.length==0){
         alert("数据生成中！");
       }
    });
 }
}
