import { Component, OnInit } from '@angular/core';
import { NzModalService } from 'ng-zorro-antd';
import {NzMessageService} from 'ng-zorro-antd';
import {HttpService} from '../../services/http/http.service';
import { ActivatedRoute} from '@angular/router';
declare var $:any;
@Component({
  selector: 'app-concept-card',
  templateUrl: './concept-card.component.html',
  styleUrls: ['./concept-card.component.sass']
})
export class ConceptCardComponent implements OnInit {
  baseUrl=''
  isVisible = false;
  isVisible1 = false;
  cardList:Array<any>=[];
  card={
    CardId:'',
    CardOrder:'',
    ProjectAutoId:0,
    ConceptCardName:'',
    ConceptCardDesc:''
  }
  image:any;
  constructor(
    private confirmServ: NzModalService,
    private route: ActivatedRoute,
    private http: HttpService,
    private _message: NzMessageService
  ) { }

  /**
   * 获取概念卡列表
   */
  ngOnInit() {
    this.baseUrl=this.http.baseUrl;
    this.card.ProjectAutoId=this.route.snapshot.params['id'];
    this.getCardList();
  }
  showModal = (card) => {
    console.log(card)
    if(!card){
      this.card.ConceptCardName='';
      this.card.ConceptCardDesc='';
      this.card.CardId='';
    }
    else {
      this.card.CardId=card.CardId;
      this.card.CardOrder=card.CardOrder;
      this.card.ConceptCardName=card.ConceptCardName
      this.card.ConceptCardDesc=card.ConceptCardDesc
    }
    this.isVisible = true;
  }
  showModal1(image){
    this.isVisible1=true;
    this.image=image;
  }
  handleCancel(){
    this.isVisible=false;
    this.isVisible1=false;
  }
  handleOk1(){
    this.isVisible1=false;
  }
  /**
   * 获取概念卡
   */
  getCardList(){
    let that=this;
    this.http.request({url:`/api/ConceptCard/GetConceptCardList?projectAutoId=${this.card.ProjectAutoId}`,method:'get'},null).then(data=>{
      data=JSON.parse(data._body)
      if(data.Result==0){
        that.cardList=data.data;
      }
    })
  }
  /**
   * 删除概念卡
   */
  showConfirm = (card) => {
    let that=this;
    this.confirmServ.confirm({
      title  : '您是否确认要删除这项内容',
      content: '<b>删除的内容将不再显示</b>',
      onOk() {
        console.log('确定');
        that.http.request({url:`/api/ConceptCard/DelConceptCard?cardId=${card.CardId}`,method:'get'},null).then(data=>{
          data=JSON.parse(data._body)
          if(data.Result==0){
            that._message.success('成功', {nzDuration: 2000});
            that.getCardList();
          } else{
            that._message.warning(data.Message, {nzDuration: 2000});
          }
        })
      }
    });
  }
  /**
   * 添加、编辑概念卡
   * @param e
   */
  handleOk = () => {
      let that=this;
      this.http.request({url:`/api/ConceptCard/AddOrUpdateCard`,method:'post'},this.card).then(data=>{
        data=JSON.parse(data._body)
        if(data.Result==0){
          that._message.success('成功', {nzDuration: 2000});
          that.isVisible = false;
          that.getCardList();
        } else{
          that.isVisible = false;
          that._message.warning(data.Message, {nzDuration: 2000});
        }
      })
  }

  /**
   * 上传概念卡图片
   * @param card
   */
  uploadCardImg(card,e,i){
    var formData = new FormData();
    formData.append("file",$(`#fileupload${i}`)[0].files[0]);
    let that=this;
    $.ajax({
      url : this.baseUrl+`/api/ConceptCard/UploadCardImage?projectAutoId=${this.card.ProjectAutoId}&cardId=${card.CardId}`,
      type : 'POST',
      data : formData,
      processData : false,// 告诉jQuery不要去处理发送的数据
      contentType : false,// 告诉jQuery不要去设置Content-Type请求头
      success:function(data) {
        if(data.Result==0){
          that._message.success('上传成功', {nzDuration: 2000});
          that.getCardList();
        }else{
          that._message.warning(data.Message, {nzDuration: 2000});
        }
      },
      error:function(data) {
        that._message.warning(data.Message, {nzDuration: 2000});
      }
    });
  }
}
