import {Pipe, PipeTransform} from '@angular/core';
@Pipe({name:'to'})
export class ToPipe implements PipeTransform{
    transform(items:any[],index:number){
        if(!items)
        return [];
        if(items.constructor == Array && items.length>0)
        return items.slice(0,index);

    }
}