import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {HomeRoute} from '../../router/home-route';
import { NgZorroAntdModule } from 'ng-zorro-antd';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { ConceptCardComponent } from '../../components/concept-card/concept-card.component';
import { ForumGroupComponent } from '../../components/forum-group/forum-group.component';
import { ScoreAndEmoticonComponent } from '../../components/score-and-emoticon/score-and-emoticon.component';
import { ConfigSortComponent } from '../../components/config-sort/config-sort.component';
import { ConfigHeapComponent } from '../../components/config-heap/config-heap.component';
import { ReportDetailComponent } from '../../components/report-detail/report-detail.component';
import {HomeComponent} from '../../components/home/home.component';
import {HeaderComponent } from '../../components/header/header.component';
import { FileUploadModule } from 'ng2-file-upload';
import { FooterComponent } from '../../components/footer/footer.component';
@NgModule({
  imports: [
    CommonModule,
    HomeRoute,
    NgZorroAntdModule,
    FormsModule,
    FileUploadModule,
    ReactiveFormsModule
  ],
  declarations: [
    HomeComponent,
    ConceptCardComponent,
    ForumGroupComponent,
    ScoreAndEmoticonComponent,
    ConfigSortComponent,
    ConfigHeapComponent,
    ReportDetailComponent,
    HeaderComponent,
    FooterComponent
  ]
})
export class HomeModule { }
