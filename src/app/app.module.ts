import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {Routing} from './router/router';
import { HttpModule, JsonpModule } from '@angular/http';
import { FormsModule , ReactiveFormsModule} from '@angular/forms';
import { NgZorroAntdModule } from 'ng-zorro-antd';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NoopAnimationsModule} from '@angular/platform-browser/animations'

import { AppComponent } from './app.component';
import { LoginComponent } from './components/login/login.component';
import {PagenotfoundComponent} from './components/pagenotfound/pagenotfound.component';
import {GuardService} from './services/guard/guard.service';
import {HttpService} from './services/http/http.service';
import {HashLocationStrategy, LocationStrategy} from "@angular/common";
import { UploadComponent } from './components/upload/upload.component';
import { AppenduserComponent } from './components/appenduser/appenduser.component';
import { NgUploaderModule } from 'ngx-uploader';
import { WordcloudComponent } from './components/wordcloud/wordcloud.component';
import { TableComponent } from './components/table/table.component';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { ToPipe } from './app.pipe.to';
import { DashboardService } from './components/dashboard/dashboard.service';
import { CardComponent } from './components/card/card.component';
import {NgxEchartsModule} from 'ngx-echarts';


@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    PagenotfoundComponent,
    UploadComponent,
    AppenduserComponent,
    WordcloudComponent,
    TableComponent,
    DashboardComponent,
    ToPipe,
    CardComponent
  ],
  imports: [
    Routing,
    HttpModule,
    FormsModule,
    BrowserModule,
    ReactiveFormsModule,
    NgUploaderModule,
    NoopAnimationsModule,
    NgZorroAntdModule.forRoot(),
    NgxEchartsModule
  ],
  providers: [
    GuardService,
    HttpService,
    DashboardService,
    {
    provide:LocationStrategy,
    useClass:HashLocationStrategy
  }],
  bootstrap: [AppComponent]
})
export class AppModule { }
