import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';
import {GuardService} from '../services/guard/guard.service';
import {LoginComponent} from '../components/login/login.component';
import {HomeComponent} from '../components/home/home.component';
import {ConceptCardComponent} from '../components/concept-card/concept-card.component';
import {ForumGroupComponent} from '../components/forum-group/forum-group.component';
import {ScoreAndEmoticonComponent} from '../components/score-and-emoticon/score-and-emoticon.component';
import {ConfigSortComponent} from '../components/config-sort/config-sort.component';
import {ConfigHeapComponent} from '../components/config-heap/config-heap.component';
import {ReportDetailComponent} from '../components/report-detail/report-detail.component';
const routes: Routes = [
  {path: '', component: HomeComponent},
  {path: 'concept-card/:id', component: ConceptCardComponent, data: { title: '配置概念卡' }},
  {path: 'forum-group/:id', component: ForumGroupComponent, data: { title: '配置座谈会组' }},
  {path: 'score-and-emotion/:id', component: ScoreAndEmoticonComponent, data: { title: '配置打分/表情包' }},
  {path: 'config-sort/:id', component: ConfigSortComponent, data: { title: '配置排序' }},
  {path: 'config-heap/:id', component: ConfigHeapComponent, data: { title: '配置分堆' }},
  {path: 'report/:id', component: ReportDetailComponent, data: { title: '查看报告' }},
]
@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [
    RouterModule
  ]
})
export class HomeRoute {}
