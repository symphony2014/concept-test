import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';
import {GuardService} from '../services/guard/guard.service';
import {LoginComponent} from '../components/login/login.component';
import {PagenotfoundComponent} from '../components/pagenotfound/pagenotfound.component';
import { UploadComponent } from '../components/upload/upload.component';
import { WordcloudComponent } from '../components/wordcloud/wordcloud.component';
import { TableComponent } from '../components/table/table.component';
import { DashboardComponent } from '../components/dashboard/dashboard.component';
import { AppenduserComponent } from '../components/appenduser/appenduser.component';
const routes: Routes = [
  {
    path: '',
    redirectTo: 'login',
    pathMatch: 'full',
  },
  {
    path: 'login',
    component: LoginComponent,
    data: { title: '登录' }
  },
  {
    path: 'appenduser',
    component: AppenduserComponent,
    data: { title: '添加用户' }
  },
  {
    path: 'upload',
    component: UploadComponent,
    data: { title: '上传文件' }
  },
  {
    path: 'table',
    component: TableComponent,
    data: { title: '聊天信息表格' }
  },
  {
    path: 'wordcloud',
    component: WordcloudComponent,
    data: { title: '聊天信息词云' }
  },{
    
    path: 'dashboard',
    component: DashboardComponent,
    data: { title: '聊天信息词云' }
  },
  {
    path: 'home',
    loadChildren: '../modules/home/home.module#HomeModule',
    canActivate: [GuardService],
    data: { title: '首页' }
  },
  {path: '**', component: PagenotfoundComponent}
]
@NgModule({
  imports: [
    RouterModule.forRoot(routes)
  ],
  exports: [
    RouterModule
  ]
})
export class Routing {}
